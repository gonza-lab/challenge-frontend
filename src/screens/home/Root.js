import React from 'react';
import { useSelector } from 'react-redux';
import { PostBar } from '../../components/post/bar/Bar';
import { PostList } from '../../components/post/list/List';
import './Root.scss';

export const ScreenHomeRoot = () => {
  const { isAuth } = useSelector((state) => state.user);

  return (
    <div className="home-root">
      {isAuth && <PostBar />}
      <PostList />
    </div>
  );
};
