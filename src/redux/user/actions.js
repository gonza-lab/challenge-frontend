import types from '../types';
import axios from 'axios';
import ui from '../ui/actions';

const login = (payload) => ({
  type: types.USER_LOG_IN,
  payload,
});

const logout = () => ({
  type: types.USER_LOG_OUT,
});

const startLogin = (user) => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post('/auth/login', user);
      localStorage.setItem('token', data.jwt);
      dispatch(login({ id: data.id }));
      dispatch(ui.toggleModalLogin());
    } catch ({ response }) {
      if (response.status === 401) {
        dispatch(ui.setErrorLoginModal('Incorrect password'));
      } else if (response.status === 404) {
        dispatch(ui.setErrorLoginModal('There is no user with that email'));
      } else {
        dispatch(
          ui.setErrorLoginModal('An error has occurred, please try again')
        );
      }
    }
  };
};

const startSignUp = (user) => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post('/auth', user);
      localStorage.setItem('token', data.jwt);
      dispatch(login({ id: data.id }));
      dispatch(ui.toggleModalSignUp());
    } catch ({ response }) {
      console.log(response);
      if (response.status === 400) {
        dispatch(ui.setErrorSignUpModal('The email you sent is busy.'));
      }
    }
  };
};

const actions = { startLogin, startSignUp, logout, login };

export default actions;
