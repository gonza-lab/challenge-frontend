import types from '../types';

const toggleModalLogin = () => ({
  type: types.UI_TOGGLE_MODAL_LOGIN,
});

const toggleModalSignUp = () => ({
  type: types.UI_TOGGLE_MODAL_SIGN_UP,
});

const setErrorLoginModal = (payload) => ({
  type: types.UI_SET_ERROR_LOGIN_MODAL,
  payload,
});

const setErrorSignUpModal = (payload) => ({
  type: types.UI_SET_ERROR_SIGN_UP_MODAL,
  payload,
});

const toggleModalAddPost = () => ({
  type: types.UI_TOGGLE_MODAL_ADD_POST,
});

const setErrorAddPostModal = (payload) => ({
  type: types.UI_SET_ERROR_ADD_MODAL,
  payload,
});

const ui = {
  toggleModalLogin,
  toggleModalSignUp,
  toggleModalAddPost,
  setErrorLoginModal,
  setErrorSignUpModal,
  setErrorAddPostModal,
};

export default ui;
