const login = {
  email: {
    required: 'You must enter your email',
    pattern: 'You must enter a valid email.',
  },
  password: {
    required: 'You must enter your password',
  },
};

const signup = {
  email: {
    required: 'You must enter your email',
    pattern: 'You must enter a valid email.',
  },
  password: {
    required: 'You must enter your password',
  },
  repassword: {
    required: 'You must repeat your password',
    validate: 'Passwords must be the same',
  },
};

export { login, signup };
