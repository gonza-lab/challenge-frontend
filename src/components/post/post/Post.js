import React from 'react';
import './Post.scss';
import moment from 'moment';

export const Post = ({ post }) => {
  console.log(post);
  return (
    <div className="post">
      {post ? (
        <>
          <div className="post__header">
            <h1>{post?.title}</h1>
            <div>
              Posted by {post?.User?.email.split('@')[0]} |{' '}
              {moment(post?.createdAt).fromNow()}
            </div>
          </div>
          <p>{post?.body}</p>
          {post?.img && (
            <div className="post__img">
              <img alt="" src={post.img} />
            </div>
          )}
        </>
      ) : (
        <div className="post__not-found">
          Oh no ... you seem like you're looking for her love
        </div>
      )}
    </div>
  );
};
