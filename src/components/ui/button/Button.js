import React from 'react';
import './Button.scss';

export const UiButton = ({ children, secondary, ...rest }) => {
  return (
    <button
      className={'ui-button' + (secondary ? ' ui-button__secondary' : '')}
      {...rest}
    >
      {children}
    </button>
  );
};
