import React, { useCallback } from 'react';
import { useForm } from 'react-hook-form';
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';
import ui from '../../../redux/ui/actions';
import { UiButton } from '../../ui/button/Button';
import { UiInput } from '../../ui/input/Input';
import { login } from '../../../errors/auth/errors';
import user from '../../../redux/user/actions';

Modal.setAppElement('#root');

export const AuthLoginModal = () => {
  const { isOpenModalLogin, errorLoginModal } = useSelector(
    (state) => state.ui
  );
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm();

  const toggleModal = useCallback(() => {
    dispatch(ui.toggleModalLogin());
  }, [dispatch]);

  const onSubmit = (data) => {
    dispatch(user.startLogin(data));
  };

  return (
    <Modal
      isOpen={isOpenModalLogin}
      onRequestClose={toggleModal}
      className="Modal"
    >
      <svg
        viewBox="0 0 20 20"
        xmlns="http://www.w3.org/2000/svg"
        className="_14dkERGUnSwisNWFcFX-0T"
        onClick={toggleModal}
      >
        <polygon
          fill="inherit"
          points="11.649 9.882 18.262 3.267 16.495 1.5 9.881 8.114 3.267 1.5 1.5 3.267 8.114 9.883 1.5 16.497 3.267 18.264 9.881 11.65 16.495 18.264 18.262 16.497"
        ></polygon>
      </svg>
      <h1>Login</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <UiInput
          refComponent={register({
            required: true,
            pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          })}
          name="email"
          label="EMAIL"
          error={login.email[errors.email?.type]}
        />
        <UiInput
          refComponent={register({ required: true })}
          name="password"
          type="password"
          label="PASSWORD"
          error={login.password[errors.password?.type]}
        />
        <div className="form__button-submit">
          <UiButton>Log In</UiButton>
        </div>
      </form>
      {errorLoginModal && <div className="modal__error">{errorLoginModal}</div>}
    </Modal>
  );
};
