import types from '../types';

const initialState = {
  isAuth: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.USER_LOG_IN:
      return { ...state, isAuth: true, ...payload };

    case types.USER_LOG_OUT:
      return { isAuth: false };
    default:
      return state;
  }
};

export default reducer;
