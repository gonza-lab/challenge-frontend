import React, { useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { Post } from '../../components/post/post/Post';
import './Root.scss';

export const ScreenPostRoot = () => {
  const location = useLocation();
  const { list } = useSelector((state) => state.post);
  const [isLooking, setIsLookin] = useState(true);

  const post = useMemo(() => {
    let wantedPost;

    if (list.length) {
      const id = location.pathname.split('/')[3];
      wantedPost = list.find((post) => post.id === +id);
      setIsLookin(false);
    }

    return wantedPost;
  }, [list, location.pathname]);

  return <div className="post-root">{!isLooking && <Post post={post} />}</div>;
};
