import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import post from '../../../redux/post/actions';
import { add } from '../../../errors/post/errors';
import { UiButton } from '../../ui/button/Button';
import { UiInput } from '../../ui/input/Input';
import { UiTextArea } from '../../ui/textarea/Textarea';
import './Card.scss';

export const PostAddCard = () => {
  const dispatch = useDispatch();
  const { errorAddModal } = useSelector((state) => state.ui);
  const { register, handleSubmit, errors } = useForm();
  const history = useHistory();

  const onSubmit = (data) => {
    dispatch(post.startAdd(data, history));
  };

  return (
    <div className="post-add-card">
      <h1>Add post</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <UiInput
          name="title"
          refComponent={register({ required: true })}
          label="Title"
          error={add.title[errors.title?.type]}
        />
        <UiInput
          name="img"
          refComponent={register({
            required: true,
            pattern: /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi,
          })}
          label="Image"
          error={add.img[errors.img?.type]}
        />
        <UiInput
          name="category"
          refComponent={register({ required: true })}
          label="Category"
          error={add.category[errors.category?.type]}
        />
        <UiTextArea
          name="body"
          refComponent={register({ required: true })}
          label="Body"
          error={add.body[errors.body?.type]}
        />
        <div className="form__button-submit">
          <UiButton>Add</UiButton>
        </div>
      </form>
      {errorAddModal && <div className="modal__error">{errorAddModal}</div>}
    </div>
  );
};
