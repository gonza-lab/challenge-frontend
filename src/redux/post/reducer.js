import types from '../types';

const initialState = {
  list: [],
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.POST_ADD:
      return { ...state, list: [payload, ...state.list] };

    case types.POST_GET:
      return { ...state, list: [...state.list, ...payload] };

    case types.POST_DELETE:
      return {
        ...state,
        list: state.list.reduce(
          (acum, curr) => (curr.id === payload.id ? acum : [...acum, curr]),
          []
        ),
      };

    case types.POST_EDIT:
      return {
        ...state,
        list: state.list.reduce(
          (acum, curr) =>
            curr.id === payload.id ? [...acum, payload] : [...acum, curr],
          []
        ),
      };

    default:
      return state;
  }
};

export default reducer;
