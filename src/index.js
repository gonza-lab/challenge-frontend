import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import { ScreenRoot } from './screens/Root';
import './styles/general.scss';

ReactDOM.render(
  <Provider store={store}>
    <ScreenRoot />
  </Provider>,
  document.getElementById('root')
);
