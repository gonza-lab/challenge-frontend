import React from 'react';
import moment from 'moment';
import ReactTooltip from 'react-tooltip';
import { useDispatch, useSelector } from 'react-redux';
import postActions from '../../../redux/post/actions';
import './Item.scss';
import { useHistory } from 'react-router';

export const PostItem = ({ post }) => {
  const { id } = useSelector((state) => state.user);
  const history = useHistory();
  const dispatch = useDispatch();

  return (
    <div className="post-item">
      <div
        onClick={() => history.push(`/posts/detail/${post.id}`)}
        className="post-item__info"
      >
        <h3 className="post-item__title">{post.title}</h3>
        <div className="post-item__date">
          Posted by {post.User?.email.split('@')[0]} |{' '}
          {moment(post.createdAt).fromNow()}
        </div>
      </div>
      {id === post.User?.id && (
        <>
          <button
            data-tip
            data-for={`actions-post-${post.id}`}
            data-event="click"
            data-event-off="blur"
          >
            <i className="fas fa-ellipsis-h"></i>
          </button>
          <ReactTooltip
            backgroundColor="transparent"
            clickable
            id={`actions-post-${post.id}`}
            className="post-item__tooltip-wrapper"
          >
            <button onClick={() => dispatch(postActions.startRemove(post))}>
              Delete
            </button>
            <button onClick={() => history.push(`/posts/edit/${post.id}`)}>
              Edit
            </button>
          </ReactTooltip>
        </>
      )}
    </div>
  );
};
