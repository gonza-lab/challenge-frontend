import React, { useState } from 'react';
import './Textarea.scss';

export const UiTextArea = ({ writed, label, refComponent, error, ...rest }) => {
  const [isFocus, setIsFocus] = useState(false);
  const [isWrited, setIsWrited] = useState(false);

  return (
    <div
      className={
        'ui-text-area' +
        (isFocus || isWrited || writed ? ' ui-text-area__focused' : '')
      }
    >
      <label>{label}</label>
      <textarea
        onChange={(e) => {
          setIsWrited(Boolean(e.target.value));
          if (rest.onChange) rest.onChange();
        }}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        {...rest}
        ref={refComponent}
      ></textarea>
      <span>{error}</span>
    </div>
  );
};
