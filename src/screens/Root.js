import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import post from '../redux/post/actions';
import useRenewToken from '../hooks/useRenewToken';
import { AuthLoginModal } from '../components/auth/login/Modal';
import { Navbar } from '../components/navbar/Navbar';
import { AuthSignUpModal } from '../components/auth/signup/Modal';
import { ScreenHomeRoot } from './home/Root';
import { PostAddCard } from '../components/post/addcard/Card';
import { RouteConditional } from '../components/route/Conditional';
import { ScreenPostRoot } from '../screens/post/Root';
import { PostEditCard } from '../components/post/editcard/Card';
import './Root.scss';
import '../styles/modal.scss';

export const ScreenRoot = () => {
  const { isAuth } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  useRenewToken();

  useEffect(() => dispatch(post.startLoadAll()), [dispatch]);

  return (
    <Router>
      <div className="screen-root">
        <Navbar />
        <main>
          <Switch>
            <Route exact path="/">
              <ScreenHomeRoot />
            </Route>
            <RouteConditional
              exact
              conditionToOpen={isAuth}
              path="/posts/add"
              pathRedirect="/"
              component={PostAddCard}
            />
            <RouteConditional
              exact
              conditionToOpen={isAuth}
              path="/posts/edit/:id"
              pathRedirect="/"
              component={PostEditCard}
            />
            <Route exact path="/posts/detail/:id">
              <ScreenPostRoot />
            </Route>
            <Redirect to="/" />
          </Switch>
        </main>
        <AuthLoginModal />
        <AuthSignUpModal />
      </div>
    </Router>
  );
};
