import types from '../types';

const initialState = {
  isOpenModalLogin: false,
  isOpenModalSignUp: false,
  isOpenModalAddPost: false,
  errorLoginModal: '',
  errorSignUpModal: '',
  errorAddModal: '',
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.UI_TOGGLE_MODAL_LOGIN:
      return {
        ...state,
        isOpenModalLogin: !state.isOpenModalLogin,
        errorLoginModal: '',
      };

    case types.UI_TOGGLE_MODAL_SIGN_UP:
      return {
        ...state,
        isOpenModalSignUp: !state.isOpenModalSignUp,
        errorSignUpModal: '',
      };

    case types.UI_TOGGLE_MODAL_ADD_POST:
      return {
        ...state,
        isOpenModalAddPost: !state.isOpenModalAddPost,
        errorAddModal: '',
      };

    case types.UI_SET_ERROR_LOGIN_MODAL:
      return { ...state, errorLoginModal: payload };

    case types.UI_SET_ERROR_SIGN_UP_MODAL:
      return { ...state, errorSignUpModal: payload };

    case types.UI_SET_ERROR_ADD_MODAL:
      return { ...state, errorAddModal: payload };

    default:
      return state;
  }
};

export default reducer;
