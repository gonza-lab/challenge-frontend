import React, { useCallback } from 'react';
import { useHistory } from 'react-router';
import { UiButton } from '../../ui/button/Button';
import './Bar.scss';

export const PostBar = () => {
  const history = useHistory();

  const toggleModalAddPost = useCallback(() => {
    history.push('/posts/add')
  }, [history]);

  return (
    <div className="post-bar">
      <div>Posts</div>
      <div className="post-bar__buttons">
        <div>
          <UiButton onClick={toggleModalAddPost}>Add post</UiButton>
        </div>
      </div>
    </div>
  );
};
