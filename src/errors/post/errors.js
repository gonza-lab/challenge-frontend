const add = {
  title: {
    required: 'You must enter a title.',
  },
  img: {
    required: 'You must enter a image.',
    pattern: 'You must enter a valid url image.',
  },
  category: {
    required: 'You must enter a category.',
  },
  body: {
    required: 'You must enter a body.',
  },
};

export { add };
