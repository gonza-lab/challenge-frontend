import React, { useEffect, useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import postActions from '../../../redux/post/actions';
import { add } from '../../../errors/post/errors';
import { UiButton } from '../../ui/button/Button';
import { UiInput } from '../../ui/input/Input';
import { UiTextArea } from '../../ui/textarea/Textarea';
import './Card.scss';

export const PostEditCard = () => {
  const dispatch = useDispatch();
  const { list } = useSelector((state) => state.post);
  const { register, handleSubmit, errors, reset } = useForm();
  const history = useHistory();
  const location = useLocation();

  const onSubmit = (data) => {
    dispatch(postActions.startUpdate({ ...data, id: post.id }, history));
  };

  const post = useMemo(() => {
    const id = location.pathname.split('/')[3];
    let postToUpdate = list.find((post) => post.id === +id);

    return postToUpdate;
  }, [list, location.pathname]);

  useEffect(() => {
    const { title, body, img, Category } = post;
    reset({ title, body, img, category: Category.name });
  }, [post, reset]);

  return (
    <div className="post-add-card">
      <h1>Edit post</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <UiInput
          name="title"
          refComponent={register({ required: true })}
          label="Title"
          error={add.title[errors.title?.type]}
          writed
        />
        <UiInput
          name="img"
          refComponent={register({
            required: true,
            pattern: /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi,
          })}
          label="Image"
          error={add.img[errors.img?.type]}
          writed
        />
        <UiInput
          name="category"
          refComponent={register({ required: true })}
          label="Category"
          error={add.category[errors.category?.type]}
          writed
        />
        <UiTextArea
          name="body"
          refComponent={register({ required: true })}
          writed
          label="Body"
          error={add.body[errors.body?.type]}
        />
        <div className="form__button-submit">
          <UiButton>Update</UiButton>
        </div>
      </form>
    </div>
  );
};
