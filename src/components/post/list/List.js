import React from 'react';
import { useSelector } from 'react-redux';
import { PostItem } from '../item/Item';
import './List.scss';

export const PostList = () => {
  const { list } = useSelector((state) => state.post);

  return (
    <div className="post-list">
      {list.map((post) => (
        <PostItem post={post} />
      ))}
    </div>
  );
};
