import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import user from '../redux/user/actions';
import axios from 'axios';

const useRenewToken = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const token = localStorage.getItem('token');
    (async () => {
      if (token) {
        try {
          const { data } = await axios.get('/auth/renew', {
            headers: { authorization: 'Bearer ' + token },
          });
          dispatch(user.login({ id: data.id }));
        } catch (error) {}
      }
    })();
  }, [dispatch]);
};

export default useRenewToken;
