import types from '../types';
import axios from 'axios';
import ui from '../ui/actions';

const add = (payload) => ({
  type: types.POST_ADD,
  payload,
});

const loadAll = (payload) => ({
  type: types.POST_GET,
  payload,
});

const remove = (payload) => ({
  type: types.POST_DELETE,
  payload,
});

const update = (payload) => ({
  type: types.POST_EDIT,
  payload,
});

const startAdd = (post, history) => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post('/posts', post, {
        headers: {
          authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      });
      const { ok, ...newPost } = data;

      dispatch(add(newPost));
      history.push('/');
    } catch ({ response }) {
      dispatch(
        ui.setErrorAddPostModal('An error has occurred, please try again')
      );
    }
  };
};

const startLoadAll = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.get('/posts');
      dispatch(loadAll(data));
    } catch (error) {}
  };
};

const startRemove = (post) => {
  return async (dispatch) => {
    try {
      await axios.delete(`/posts/${post.id}`, {
        headers: { authorization: 'Bearer ' + localStorage.getItem('token') },
      });
      dispatch(remove(post));
    } catch (error) {}
  };
};

const startUpdate = ({ id, ...post }, history) => {
  return async (dispatch) => {
    try {
      const { data } = await axios.patch(`/posts/${id}`, post, {
        headers: { authorization: 'Bearer ' + localStorage.getItem('token') },
      });
      const { ok, ...postUpdated } = data;

      dispatch(update(postUpdated));
      history.push('/');
    } catch ({ response }) {}
  };
};

const actions = { startAdd, startLoadAll, startRemove, startUpdate };

export default actions;
