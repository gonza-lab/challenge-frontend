import React, { useCallback } from 'react';
import { useForm } from 'react-hook-form';
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';
import ui from '../../../redux/ui/actions';
import user from '../../../redux/user/actions';
import { UiButton } from '../../ui/button/Button';
import { UiInput } from '../../ui/input/Input';
import { signup } from '../../../errors/auth/errors';

Modal.setAppElement('#root');

export const AuthSignUpModal = () => {
  const { isOpenModalSignUp, errorSignUpModal } = useSelector(
    (state) => state.ui
  );
  const dispatch = useDispatch();
  const { register, handleSubmit, errors, watch } = useForm();

  const toggleModal = useCallback(() => {
    dispatch(ui.toggleModalSignUp());
  }, [dispatch]);

  const onSubmit = ({ email, password }) => {
    dispatch(user.startSignUp({ email, password }));
  };

  return (
    <Modal
      isOpen={isOpenModalSignUp}
      onRequestClose={toggleModal}
      className="Modal"
    >
      <svg
        viewBox="0 0 20 20"
        xmlns="http://www.w3.org/2000/svg"
        className="_14dkERGUnSwisNWFcFX-0T"
        onClick={toggleModal}
      >
        <polygon
          fill="inherit"
          points="11.649 9.882 18.262 3.267 16.495 1.5 9.881 8.114 3.267 1.5 1.5 3.267 8.114 9.883 1.5 16.497 3.267 18.264 9.881 11.65 16.495 18.264 18.262 16.497"
        ></polygon>
      </svg>
      <h1>Sign Up</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <UiInput
          name="email"
          refComponent={register({
            required: true,
            pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
          })}
          label="EMAIL"
          error={signup.email[errors.email?.type]}
        />
        <UiInput
          name="password"
          type="password"
          refComponent={register({ required: true })}
          label="PASSWORD"
          error={signup.password[errors.password?.type]}
        />
        <UiInput
          name="repassword"
          type="password"
          refComponent={register({
            required: true,
            validate: (value) => value === watch('password'),
          })}
          label="CONFIRM YOUR PASS"
          error={signup.repassword[errors.repassword?.type]}
        />
        <div className="form__button-submit">
          <UiButton>Sign Up</UiButton>
        </div>
      </form>
      {errorSignUpModal && (
        <div className="modal__error">{errorSignUpModal}</div>
      )}
    </Modal>
  );
};
