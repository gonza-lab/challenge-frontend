import React, { useState } from 'react';
import './Input.scss';

export const UiInput = ({ writed, label, refComponent, error, ...rest }) => {
  const [isFocus, setIsFocus] = useState(false);
  const [isWrited, setIsWrited] = useState(false);

  return (
    <div
      className={
        'ui-input' + (isFocus || isWrited || writed ? ' ui-input__focused' : '')
      }
    >
      <label>{label}</label>
      <input
        onChange={(e) => {
          setIsWrited(Boolean(e.target.value));
          if (rest.onChange) rest.onChange();
        }}
        onFocus={() => setIsFocus(true)}
        onBlur={() => setIsFocus(false)}
        {...rest}
        ref={refComponent}
      />
      <span>{error}</span>
    </div>
  );
};
