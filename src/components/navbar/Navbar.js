import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import ui from '../../redux/ui/actions';
import user from '../../redux/user/actions';
import { UiButton } from '../ui/button/Button';
import './Navbar.scss';

export const Navbar = () => {
  const dispatch = useDispatch();
  const { isAuth } = useSelector((state) => state.user);

  const toggleModalLogin = useCallback(() => {
    dispatch(ui.toggleModalLogin());
  }, [dispatch]);

  const toggleModalSignUp = useCallback(() => {
    dispatch(ui.toggleModalSignUp());
  }, [dispatch]);

  const logout = useCallback(() => {
    localStorage.removeItem('token');
    dispatch(user.logout());
  }, [dispatch]);

  return (
    <nav className="navbar">
      <Link to="/">
        <div className="navbar__brand">ReactPost</div>
      </Link>
      <div className="navbar__auth">
        {!isAuth ? (
          <>
            <UiButton secondary onClick={toggleModalLogin}>
              Log In
            </UiButton>
            <UiButton onClick={toggleModalSignUp}>Sign Up</UiButton>
          </>
        ) : (
          <UiButton onClick={logout}>Log Out</UiButton>
        )}
      </div>
    </nav>
  );
};
